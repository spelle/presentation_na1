[.lightbg,background-image="fond_diapo_skieur.jpg",background-opacity="0.6"]
== Facteurs agravants et Déclenchement

[background-image="fond_diapo_skieur.jpg",background-opacity="0.6"]
=== Météorologie

image::facteur_meteo.png[width=200px]

[.small.fragment.left]
--
icon:angle-right[] Une grande quantitée de neige fraiche associée à du vent et des températures froides.
--

[background-image="fond_diapo_skieur.jpg",background-opacity="0.6"]
=== Topographie

image::facteur_topo.png[width=200px]

[.small.fragment.left]
--
icon:angle-right[] Pente vierge et raide. 30° et plus. +
icon:angle-right[] Corniche, creux, crêtes et rupture de pente +
icon:angle-right[] Orientation de la pente
--

[background-image="fond_diapo_skieur.jpg",background-opacity="0.6"]
=== Manteau Neigeux

image::facteur_manteau.png[width=200px]

[.small.fragment.left]
--
icon:angle-right[] Une plaque de neige se détache lorsqu’une couche fragile est présente dans le manteau neigeux.
--

[background-image="fond_diapo_skieur.jpg",background-opacity="0.6"]
=== Facteur Humain

image::facteur_humain.png[width=200px]

[.small.fragment.left]
--
icon:angle-right[] Même si l’excitation est grande, on doit toujours agir de façon responsable et rester sur la défensive. Le risque existant ici ne peut pas être anticipé.
--

[background-image="fond_diapo_skieur.jpg",background-opacity="0.6"]
=== Déclenchement (1/2)

image::surcharge_skieur.png[]

[background-image="fond_diapo_skieur.jpg",background-opacity="0.6"]
=== Déclenchement (2/2)

[.stretch]
image::surcharge_skieur_photo.jpg[]
