[.lightbg,background-image="fond_diapo.jpg",background-opacity="0.6"]
== Equipement de sécurité

[background-image="fond_diapo.jpg",background-opacity="0.6"]
[.columns.is-vcentered]
=== Attention

[.column]
image::icone_attention.png[]

[.column.left]
UN *ÉQUIPEMENT DE SECURITE NE PERMET PAS* D’ÉVITER LE DÉCLENCHEMENT D’UNE AVALANCHE OU L’ENFOUISSEMENT SOUS UNE AVALANCHE.

[background-image="fond_diapo.jpg",background-opacity="0.6"]
[.columns.is-vcentered]
=== Equipement d'urgence "standard" - DVA

[.column]
image::materiel_dva.png[]

[.column.left]
icon:angle-right[] *DVA* : 3 antennes et une fonction marquage font désormais partie du standard.

[background-image="fond_diapo.jpg",background-opacity="0.6"]
[.columns.is-vcentered]
=== Equipement d'urgence "standard" - Pelle

[.column]
image::materiel_pelle.png[]

[.column.left]
icon:angle-right[] *Pelle* : La fonction pioche permet d‘économiser un temps vital.

[background-image="fond_diapo.jpg",background-opacity="0.6"]
[.columns.is-vcentered]
=== Equipement d'urgence "standard" - Sonde

[.column]
image::materiel_sonde.png[]

[.column.left]
icon:angle-right[] *Sonde* : Un système d‘assemblage rapide et robuste est essentiel. 

[background-image="fond_diapo.jpg",background-opacity="0.6"]
[.columns.is-vcentered]
=== Equipement d'urgence "étendu"

[.column]
image::materiel_sac_avalanche.png[]

[.column.left]
icon:angle-right[] *Sac à dos avalanche* : Un système Airbag peut diminuer la profondeur d’enfouissement.

[background-image="fond_diapo.jpg",background-opacity="0.6"]
[.columns.is-vcentered]
=== Equipement d'urgence "étendu"

[.column]
image::materiel_casque.png[]

[.column.left]
icon:angle-right[] *Casque* : Devrait toujours être porté sur le terrain.

[background-image="fond_diapo.jpg",background-opacity="0.6"]
[.columns.is-vcentered]
=== Equipement d'urgence pour un groupe

[.column]
image::materiel_trousse_secours.png[]

[.column.left]
icon:angle-right[] *SET DE PREMIERS SECOURS ET COUVERTURE DE SURVIE* : Peuvent sauver la vie et rendre les situations d’urgence plus supportables.

[background-image="fond_diapo.jpg",background-opacity="0.6"]
[.columns.is-vcentered]
=== Equipement d'urgence pour un groupe

[.column]
image::materiel_mobile.png[]

[.column.left]
icon:angle-right[] *TÉLÉPHONE PORTABLE* : Vous pouvez alerter les secours en composant le 112.
*ATTENTION* à la proximité avec le DVA 20cm en émission / 50cm en réception / 25m si appel.
 
[background-image="fond_diapo.jpg",background-opacity="0.6"]
[.columns.is-vcentered]
=== Equipement complet vs Delais

[.column]
image::materiel_complet_delais.png[]

[.column.left]
icon:angle-right[] Seul un *ÉQUIPEMENT DE SÉCURITÉ AVALANCHE COMPLET* vous permet, en cas de danger, de localiser vos amis et de les sortir de l’avalanche. 
