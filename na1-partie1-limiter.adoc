[.lightbg,background-image="fond_titre.jpg",background-opacity="0.6"]
== Limiter les risques

[background-image="fond_diapo_neige_cordier.jpg",background-opacity="0.6"]
=== Comportement en montée

[.fragment.small.left]
--
icon:angle-right[] Adaptez votre itinéraire en fonction du terrain. LES GOULOTTES, CREUX (talwegs) et RUPTURES de PENTE inutiles doivent être évités car ils peuvent être dangereux.
--

[.fragment.small.left]
--
icon:angle-right[] Contournez les CONGÈRES ou zones D’ACCUMULATIONS DE NEIGE.
--

[.fragment.small.left]
--
icon:angle-right[] Restez à l’écoute des SIGNES AVANT-COUREURS.
--

[.fragment.small.left]
--
icon:angle-right[] ORIENTEZ-VOUS régulièrement, aussi bien sur le TERRAIN que sur la CARTE et vérifiez toujours l’itinéraire de vos traces.
--

[background-image="fond_diapo_neige_cordier.jpg",background-opacity="0.6"]
=== Comportement en montée

[.fragment.small.left]
--
icon:angle-right[] Vérifiez si L’INCLINAISON DE LA PENTE est adaptée au niveau de risque d’avalanches.
--

[.fragment.small.left]
--
Vous pouvez utiliser comme instrument de mesure un inclinomètre ou les bâtons de ski (méthode du pendule). Globalement, retenez qu’en montée
on passe des virages classiques aux conversions à partir de 30° d’inclinaison environ.
--

[background-image="fond_diapo_neige_cordier.jpg",background-opacity="0.6"]
=== Méthode du pendule

[.stretch]
image::limiter_pente_methode_baton.png[]

[background-image="fond_diapo_neige_cordier.jpg",background-opacity="0.6"]
=== Comportement en montée

[.fragment.small.left]
--
icon:angle-right[] LES PENTES RAIDES doivent être parcourues UN PAR UN ou en respectant un ESPACEMENT DE SÉCURITÉ d’au moins 5 – 10 m.
--

[.fragment.small.left]
--
icon:angle-right[] LES AVALANCHES FRAICHES sont des indicateurs clairs d’un niveau de risque d’avalanches élevé – soyez particulièrement prudents et respectez les DISTANCES DE SÉCURITÉ !
--

[background-image="fond_diapo_neige_cordier.jpg",background-opacity="0.6",.left]
=== Signes avant-coureurs

*Corniches*

[.small.col2]

image::limiter_corniche.jpg[width=600px]

[.small.col2]

[.fragment.left]
--
icon:angle-right[] Leur existence est un signe d'épisodes de vent fort. De dangereuses quantités de neige s’accumulent sous le vent.
--

[background-image="fond_diapo_neige_cordier.jpg",background-opacity="0.6",.left]
=== Signes avant-coureurs

*icon:exclamation-triangle[] Corniches & rupture*

[.stretch]
image::limiter_schema_corniche.gif[]

[background-image="fond_diapo_neige_cordier.jpg",background-opacity="0.6",.left]
=== Signes avant-coureurs

*Corniches & Plaques à Vent*

[.stretch]
image::limiter_schema_corniche_pav.gif[]

[background-image="fond_diapo_neige_cordier.jpg",background-opacity="0.6"]
=== Signes avant-coureurs

*Neige qui s'envole (transport)*

[.small.col2]

image::limiter_transport.jpg[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] Signe significatif de vent fort et d’accumulations de neige sous le vent.
--

[background-image="fond_diapo_neige_cordier.jpg",background-opacity="0.6"]
=== Signes avant-coureurs

*Congères & Accumulations*

[.small.col2]

image::limiter_congeres_accumulations.jpg[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] Si la neige a été transportée par le vent, des traces laissées par le vent sont souvent visibles sur la surface de la neige (par ex. traces de vagues, striures, dunes). Cette neige transportée et accumulée par le vent est l’une des principales causes d’avalanches de plaque.
--

[background-image="fond_diapo_neige_cordier.jpg",background-opacity="0.6"]
=== Signes avant-coureurs

*Activité récente*

[.small.col2]

image::limiter_activite_recente.jpg[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] Leur existence montre que le manteau neigeux est défavorable et instable. Il convient d’éviter les zones environnantes et les autres pentes ayant la même exposition à la même altitude.
--

[background-image="fond_diapo_neige_cordier.jpg",background-opacity="0.6"]
=== Signes avant-coureurs

*Déclenchements spontanés*

[.small.col2]

image::limiter_departs_spontanes.jpg[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] Ils surviennent généralement en cas de risque d’avalanche élevé. Ils sont un indicateur de danger, en particulier au printemps.
--

[background-image="fond_diapo_neige_cordier.jpg",background-opacity="0.6"]
=== Signes avant-coureurs

*Fissures*

[.small.col2]

image::limiter_fissure.jpg[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] Sur la neige signalent qu’une rupture de la couche de neige se prépare. Elles s’accompagnent souvent d’un bruit sourd ou bruit d’affaissement.
--

[background-image="fond_diapo_neige_cordier.jpg",background-opacity="0.6"]
=== Signes avant-coureurs

*Bruits carastéristiques de type "Whoom"*

[.small.col2.fragment.left]
--
icon:angle-right[] Le bruit sourd caractéristique lié à un affaissement de la couche neige signifie le plus grand danger. Evitez les pentes raides (>30°) et contournez les largement.
--

[background-image="fond_diapo_neige_cordier.jpg",background-opacity="0.6"]
=== Signes avant-coureurs

[.small.fragment]
--
icon:exclamation-triangle[] SI VOUS AVEZ DES DOUTES sur votre sécurité, la seule bonne décision est de FAIRE DEMI-TOUR!
--
