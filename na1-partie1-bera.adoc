[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== BERA

[.stretch]
image::notions_bera_risque_bera.jpg[]

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== BERA - Infos Générales

image::bera_infos_generales.jpg[]

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== BERA - Stabilité du manteau

image::bera_stabilite_manteau.jpg[]

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== BERA - Meteo

image::bera_meteo.jpg[]

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== BERA - Epaisseurs et Qualité

image::bera_epaisseur_qualite_tendance.jpg[]
