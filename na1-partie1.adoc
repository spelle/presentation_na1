// .image
// Use of the title-slide-background-image feature of reveal.js
// :include: //div[@class="slides"]
// :header_footer:

[.lightbg,background-image="fond_titre.jpg",background-opacity="0.6"]
== Neige & Avalanche (NA1), partie 1

[background-image="fond_diapo_neige_cordier.jpg",background-opacity="0.6"]
== Sommaire

[.small.fragment.left]
--
icon:angle-right[] La neige, c'est quoi ?
--

[.small.fragment.left]
--
icon:angle-right[] Les trois types d'avalanches
--

[.small.fragment.left]
--
icon:angle-right[] Facteurs agravants et déclenchement
--

[.small.fragment.left]
--
icon:angle-right[] Notions sur le BERA et niveaux de risque
--

[.small.fragment.left]
--
icon:angle-right[] Limiter le(s) risque(s)
--

include::na1-partie1-neige_c_quoi.adoc[]

include::na1-partie1-trois_type_avalanches.adoc[]

include::na1-partie1-facteur_agravants_declenchement.adoc[]

include::na1-partie1-bera_niveau_risque.adoc[]

include::na1-partie1-limiter.adoc[]

include::na1-partie1-questions.adoc[]
