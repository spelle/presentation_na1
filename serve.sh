#!/bin/sh
#
# Inital Setup see:
#    https://asciidoctor.org/docs/asciidoctor-revealjs/
#
#SRCFILE=$1.adoc
#DSTFILE=$1.html
#
#echo "Rendering $SRCFILE"
#bundle exec asciidoctor-revealjs $SRCFILE

#echo "Starting Webserver on :5000"
# python -m SimpleHTTPServer >/dev/null 2>&1 & 
ruby -run -e httpd . -p 5000 -b 127.0.0.1 2>&1 &

#firefox http://localhost:5000/$DSTFILE > /dev/null &

echo "Watching *.adoc for changes"
inotifywait -m -e close_write na1.adoc |
while read events; do
  echo $events
  bundle exec asciidoctor-revealjs $SRCFILE
done
