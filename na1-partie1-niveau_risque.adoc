[.lightbg,background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
== Niveaux de Risque

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== les 5 niveaux de risque

[.stretch]
image::notions_bera_risque_echelle_risque.png[]

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 1 - Faible

image::echelle_risque_1_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_1_pentes_a_risque.png[width=600px]

[.small.col2.left.fragment]
--
icon:angle-right[] MANTEAU NEIGEUX : bien stabilisé dans la plupart des pentes.
--

[.small.col2.left.fragment]
--
icon:angle-right[] DÉCLENCHEMENT : en général possibles que par forte surcharge sur de très rares pentes raides. Seules des coulées ou petites avalanches peuvent se produire spontanément.
--

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 1 - Faible

image::echelle_risque_1_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_1_pentes_a_risque.png[width=600px]

[.small.col2.left.fragment]
--
icon:angle-right[] RAIDEUR DE LA PENTE : RISQUE D’AVALANCHES FAIBLE, à l’exception des pentes extrêmement raides.
--

[.small.col2.left.fragment]
--
icon:angle-right[] RECOMMANDATION : Conditions en général sûres pour la randonnée.
--

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 2 - Modéré

image::echelle_risque_2_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_2_pentes_a_risque.png[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] MANTEAU NEIGEUX : que modérément stabilisé dans quelques pentes raides. Bien stabilisé, ailleurs.
--

[.small.col2.fragment.left]
--
icon:angle-right[] DÉCLENCHEMENTS : possibles surtout par forte surcharge et dans quelques pentes généralement identifiées. Des départs spontanés de grande ampleur ne sont pas à attendre.
--

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 2 - Modéré

image::echelle_risque_2_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_2_pentes_a_risque.png[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] RAIDEUR DE LA PENTE : EVITER LES PENTES EXTREMEMENT RAIDES (plus de 40°) dans toutes les orientations citées par le BRA ou qui paraissent localement dangereuses (ex. congères).
--

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 2 - Modéré

image::echelle_risque_2_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_2_pentes_a_risque.png[width=600px]

[.small.col2.fragment.left]
--
icon:exclamation-triangle[] GARDER SES DISTANCES dans les pentes qui sont raides au point qu’il est nécessaire de faire des conversions en montée.
Ici aussi : faire attention aux signaux d’alerte ou aux congères de neige fraiche
--

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 2 - Modéré

image::echelle_risque_2_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_2_pentes_a_risque.png[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] RECOMMANDATION : Conditions favorables à la randonnée, à condition de faire attention aux zones de danger localisé.
--

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 3 - Marqué

image::echelle_risque_3_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_3_pentes_a_risque.png[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] MANTEAU NEIGEUX : Dans de nombreuses pentes raides, le Manteau neigeux n‘est que modérément ou faiblement stabilisé.
--

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 3 - Marqué

image::echelle_risque_3_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_3_pentes_a_risque.png[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] DÉCLENCHEMENTS : possibles parfois même par faible surcharge et dans de nombreuses pentes.
--

[.small.col2.fragment.left]
--
icon:angle-right[] Dans certaines situations, quelques départs spontanés de taille moyenne, et parfois assez grosse, sont possibles.
--

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 3 - Marqué

image::echelle_risque_3_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_3_pentes_a_risque.png[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] RAIDEUR DE LA PENTE : EVITER LES PENTES DE PLUS DE 35° dans les situations citées dans le BRA.
--

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 3 - Marqué

image::echelle_risque_3_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_3_pentes_a_risque.png[width=600px]

[.small.col2.fragment.left]
--
icon:exclamation-triangle[] : Prudence accrue sur toutes les pentes de plus de 30° (garder ses distances, passer un par un).
--

[.small.col2.fragment.left]
--
icon:angle-right[] En cas de signes alarmants observés sur ces pentes, renoncer à la randonnée.
--

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 3 - Marqué

image::echelle_risque_3_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_3_pentes_a_risque.png[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] RECOMMANDATION : Le passage de randonneurs peut entrainer le déclenchement d’avalanches. Les possibilités de randonnée sont limitées.
--

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 4 - Fort

image::echelle_risque_4_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_4_pentes_a_risque.png[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] MANTEAU NEIGEUX : Il est faiblement stabilisé dans la plupart des pentes raides.
--

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 4 - Fort

image::echelle_risque_4_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_4_pentes_a_risque.png[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] DÉCLENCHEMENTS : probables même par faible surcharge dans de nombreuses pentes suffisamment raides. Dans certaines situations, de nombreux départs spontanés de taille moyenne, et parfois assez grosse sont à attendre.
--

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 4 - Fort

image::echelle_risque_4_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_4_pentes_a_risque.png[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] RAIDEUR DE LA PENTE : Renoncer d’une manière générale à toutes les pentes de plus de 30°. Même sur les zones plus plates, rester vigilant au risque de déclenchement spontané d’avalanches. 
--

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 4 - Fort

image::echelle_risque_4_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_4_pentes_a_risque.png[]

[.small.col2.fragment.left]
--
icon:angle-right[] RECOMMANDATION : Le passage de randonneurs risque fortement d’entrainer le déclenchement d’avalanches. Possibilités de randonnée extrêmement limitées.
--

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 5 - Fort

image::echelle_risque_5_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_5_pentes_a_risque.png[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] MANTEAU NEIGEUX : L‘instabilité du manteau neigeux est généralisée.
--

[.small.col2.fragment.left]
--
icon:angle-right[] DÉCLENCHEMENT : De nombreuses et grosses avalanches se produisant spontanément sont à attendre y compris en terrain peu raide. La promenade à ski n’est pas sûre.
--

[background-image="fond_diapo_sapins.jpg",background-opacity="0.6"]
=== Risque 5 - Fort

image::echelle_risque_5_bandeau.png[width=800]

[.small.col2]

//[.stretch]
image::echelle_risque_5_pentes_a_risque.png[width=600px]

[.small.col2.fragment.left]
--
icon:angle-right[] RAIDEUR DE LA PENTE : Les sorties en ski de randonnée et en Freeride ne sont généralement pas possibles.
--

[.small.col2.fragment.left]
--
icon:angle-right[] RECOMMANDATION : C’est l’état d’urgence, on n’a absolument rien à faire en montagne. RESTER AU CHAUD !!!!
--
